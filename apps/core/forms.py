from django import forms
from . import models
from django.contrib.auth.forms import UserCreationForm, UserChangeForm


class CustomUserCreationForm(UserCreationForm):
    first_name = forms.CharField(
        label='PRIMEIRO NOME:',
        widget=forms.TextInput(
            attrs={'class': 'form-control'}
        )
    )

    last_name = forms.CharField(
        label='ÚLTIMO NOME:',
        widget=forms.TextInput(
            attrs={'class': 'form-control'}
        )
    )

    email = forms.CharField(
        label='E-MAIL:',
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'maxlength': '100', 'placeholder': 'email@email.com.br', 'type': 'email'}
        )
    )

    telephone = forms.CharField(
        label='TELEFONE:',
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'placeholder': '(99) 9999-9999'}
        )
    )

    cell_phone = forms.CharField(
        label='CELULAR:',
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'placeholder': '(99) 99999-9999'}
        )
    )

    password1 = forms.CharField(
        label='SENHA:',
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control', 'placeholder': 'Digite a senha'
            }
        )
    )

    password2 = forms.CharField(
        label='CONFIRMAÇÃO DA SENHA:',
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control', 'placeholder': 'Digite novamente'
            }
        )
    )

    class Meta:
        model = models.MyUser
        fields = ['first_name', 'last_name', 'email', 'telephone', 'cell_phone', 'password1',
                  'password2']