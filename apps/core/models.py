import pytz
from django.db import models
from django.utils.translation import ugettext as _
from django.contrib.auth.base_user import BaseUserManager, AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin


class EmailUserManager(BaseUserManager):
    def create_user(self, *args, **kwargs):
        email = kwargs["email"]
        email = self.normalize_email(email)
        password = kwargs["password"]
        kwargs.pop("password")

        if not email:
            raise ValueError(_('Necessário um email válido'))

        user = self.model(**kwargs)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, *args, **kwargs):
        user = self.create_user(**kwargs)
        user.is_superuser = True
        user.is_staff = True
        user.save(using=self._db)
        return user


class MyUser(PermissionsMixin, AbstractBaseUser):
    first_name = models.CharField(
        verbose_name=_('Nome'), max_length=255, blank=False, help_text=_('Informe seu nome')
    )
    last_name = models.CharField(
        verbose_name=_('Sobrenome'), max_length=255, blank=False, help_text=_('Informe seu sobrenome')
    )
    email = models.EmailField(
        verbose_name=_('Email'), unique=True, max_length=255
    )
    reset_pass = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    USERNAME_FIELD = 'email'
    objects = EmailUserManager()

    def __str__(self):
        return "{} | {}".format(self.id, self.email)

    def __repr__(self):
        return "{} | {}".format(self.id, self.email)

    @property
    def ativo_human(self):
        return 'Sim' if self.is_active else 'Não'

