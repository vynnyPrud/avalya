from django.shortcuts import render, redirect
from . import forms
from . import models
from django.contrib.auth import authenticate, login
from django.contrib.auth.views import logout_then_login
from django.http import FileResponse, HttpResponse, Http404, HttpResponseRedirect, HttpResponseNotFound, JsonResponse
from django.urls import reverse, reverse_lazy


def new_usuario(request):
    if request.user.is_authenticated:
        add = False
        form = forms.CustomUserCreationForm()
        if request.user.is_authenticated:
            if request.method == 'POST':
                form = forms.CustomUserCreationForm(request.POST)
                if form.is_valid():
                    form.save()
                    add = True

        context = {
            'add': add, 'form': form
        }
        return render(request, 'registration/register.html', context)
    return redirect(
        reverse(
            'core:login_view'
        )
    )


def login_view(request):
    invalid = False
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        print('helloword')
        if user:
            login(request, user)
            return redirect('core:home_view')
        else:
            invalid = True
    context = {
        'invalid': invalid
    }
    return render(request, 'registration/login.html', context)


def user_profile(request):
    if request.user.is_authenticated:
        print('hello')
        return render(request, 'profile.html')
    return redirect('core:login_view')


def tables_view(request):
    if request.user.is_authenticated:
        return render(request, 'tables.html')
    return redirect('core:login_view')


def home_view(request):
    if request.user.is_authenticated:
        return render(request, 'home.html')
    return redirect('core:login_view')


def logout(request):
    return logout_then_login(request, login_url='/login')
