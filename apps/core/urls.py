from django.urls.conf import include
from django.urls import path
from . import views
from django.contrib.auth.views import LoginView

urlpatterns = [
    path('', views.login_view, name='login_view'),
    path('home/', views.home_view, name='home_view'),
    path('profile/', views.user_profile, name="user_profile"),
    path('login/', views.login_view, name="login_view"),
    path('tables/', views.tables_view, name="tables_view"),
    path('register/', views.new_usuario, name='new_usuario'),
    path('logout/', views.logout, name='logout'),
]
